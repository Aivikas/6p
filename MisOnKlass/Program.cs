﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    enum Sugu { Naine, Mees, Määramata = 99} // int-ist tuletatud klass 
                                             // 99 tähendab, et ühe baidina hoitakse alles määramatu sugu - standardist tulenev suurus
    class Program
    {
        static void Main(string[] args)
        {
            Inimene aivi = new Inimene(); // loon uue muutuja "Inimene"
            aivi.Nimi = "Aivi Murd"; // muutuja liikmed, millele on võimalik anda väärtus
            aivi.SünniAeg = DateTime.Parse("3.8.1977");
            aivi.Sugu = Sugu.Naine;


            Console.WriteLine(aivi.SünniAeg);
        }

    }

    class Inimene  // reference tüüpi 
    {
        public string Nimi; // andmetüüp, millest üks tüüp on int ja teine string - üks on vanus ja teine nimi
        public DateTime SünniAeg;
        public Sugu Sugu;
        public string Isikukood; // string sellepärast, et me ei kohtle seda kui arvu

        public int AnnaVanus()
        {
            int d = DateTime.Now.Year - SünniAeg.Year;
            if (SünniAeg.AddYears(d) > DateTime.Now) d--;
            return d;
        }

    }
}
